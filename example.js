const { Thyme, thyme } = require('./index.js')

const get10 = () => 10

const add10 = (x=1) => x + 10

const identity = x => x

const sum = (x, y) => x + y

const log = x => (console.log(x), x)

Thyme.all([
    thyme(identity, 500, 50)
      .then(add10)
      .then(log),
    thyme(get10, 1000)
      .then(add10)
      .then(add10)
      .then(log),
    thyme(get10, 2000)
      .then(add10, 500)
      .then(add10, 500)
      .then(log)
  ])
  .then(results => {
    return results.reduce(sum, 0)
  })
  .finally(sum => {
    console.log('sum of all values', sum)
  })

const concatWorld = x =>
  `${x} World`

const hello = thyme(concatWorld, 500, 'Hello')

const goodbye = thyme(concatWorld, 1500, 'Goodbye')

const jimmyEat = thyme(concatWorld, 3000, 'Jimmy Eat')

Thyme.all([
    hello,
    goodbye,
    jimmyEat
  ])
  .then(results => {
    return results.join(' and ')
  })
  .finally(result => {
    console.log('The result is:', result)
  })
