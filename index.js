class Thyme {
  constructor(fn, ms, args=[]) {
    if(!(this instanceof Thyme)) return new Thyme(fn, ms, args)
    let idx = 0
    let self = this
    self._fns = [[fn, ms, args]]
    self._execute = () => {
      let prevTask = self._fns[idx - 1]
      let thisTask = self._fns[idx]
      if(thisTask) {
        let fn = thisTask[0]
        let delay = thisTask[1]
        let fnArgs = prevTask ? [prevTask[2]] : (Array.isArray(args) ? args : [args])
        thisTask[3] = setTimeout(() => {
          thisTask[2] = fn(...fnArgs)
          idx ++
          self._execute()
          clearTimeout(thisTask[3])
        }, delay)
      }
      self._result = thisTask ? thisTask[2] : self._result
    }
  }
  then(fn, ms) {
    this._fns.push([fn, ms])
    return this
  }
  finally(fn, ms) {
    this._fns.push([fn, ms])
    this._execute()
  }
}

class ThymeAll {
  constructor(timeouts) {
    let idx = 0
    let self = this
    self._fns = []
    let finallyHasRun = false
    let results = []

    self._runTimeouts = () => {
      timeouts
        .forEach((to, i) => to.finally(result => {
          results[i] = result
          if(results.length === timeouts.length) {
            self._execute()
          }
        }, 0))
    }

    self._execute = () => {
      let prevTask = self._fns[idx - 1]
      let thisTask = self._fns[idx]
      if(thisTask) {
        let args = prevTask ? [prevTask[2]] : [thisTask[2]]
        if(idx === 0) {
          args = [results]
        }
        let fn = thisTask[0]
        let delay = thisTask[1]
        thisTask[3] = setTimeout(() => {
          thisTask[2] = fn(...args)
          idx ++
          self._execute()
          clearTimeout(thisTask[3])
        }, delay)
      }
    }
  }
  then(fn, ms) {
    this._fns.push([fn, ms])
    return this
  }
  finally(fn, ms) {
    this._fns.push([fn, ms])
    this._runTimeouts()
    return this
  }
}

Thyme.all = timeouts =>
  new ThymeAll(timeouts)

const thyme = (...args) =>
  new Thyme(...args)

module.exports = {
  Thyme,
  thyme
}
