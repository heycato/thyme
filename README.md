# Thyme

A way to chain setTimeouts with a promise-like syntax.

## Thyme Class

Constructs an instance of Thyme.

```
const fn = x => x

const delay = 1000 // milliseconds

const args = "Hello" // could be multiple using an array ["1", "2", "etc..."]

// same arguments as setTimeout
new Thyme(fn, delay, args)
```

Using the helper function `thyme`.

```
thyme(fn, delay, args)
```

Continue the computation with the `then` method.

```
const concatWorld = x =>
  `${x} World`

thyme(fn, delay, args)
  .then(concatWorld)

thyme(fn, delay, args)
  .then(concatWorld, 5000) // with 5 second delay
```

This still will not execute until you use the `finally` method. (it's lazy)

```
thyme(fn, delay, args)
  .then(concatWorld)
  .finally(result => {
    console.log('The result is:', result) // prints "Hello World"
  })
```

You can manage multiple Thyme computations with `Thyme.all`, similar to
Promise.all.

```
const hello = thyme(concatWorld, 500, 'Hello')

const goodbye = thyme(concatWorld, 1500, 'Goodbye')

const jimmyEat = thyme(concatWorld, 3000, 'Jimmy Eat')

Thyme.all([
    hello,
    goodbye,
    jimmyEat
  ])
  .then(results => {
    return results.join(' and ')
  })
  .finally(result => {
    // prints "Hello World and Goodbye World and Jimmy Eat World" after the last
    // timer resolves at 3000 milliseconds
    console.log('The result is:', result)
  })
```

All said, I'm not sure what this could be used for. But if you like using
setTimeout often, maybe this will help to wrangle it.



